const fs = pjs.wrap(require("fs"));
const fspath = require("path");

async function getToken(type) {
  console.log("getting RPMS Access Token.");

  let returnObj = {};
  returnObj.value = "";
  returnObj.errorMsg = "";

  //Incoming call type of 1 will point the call to Prod where as 0 will point to Dev
  let callType = type;
  // let callType = 1;

  const currentPath = __dirname + fspath.sep; // C:\tyler\modules\gorpms\tokenhandler\
  const parentPath = fspath.join(currentPath, "..") + fspath.sep; // C:\tyler\modules\gorpms\

  //DEV
  var urlData = fs.readFileSync(parentPath + "RPMS_Dev_URL_Path.json"); 
  // var urlData = fs.readFileSync(parentPath + "aditems" + fspath.sep + "RPMS_Dev_URL_Path.json"); // Adjusted path to look in aditems folder
  let file = "MF_APITokenStorage.json";
  // let url = "https://webapidev.gorpms.com/Token";

  // PROD
  if (callType === 1) {
    urlData = fs.readFileSync(parentPath + "RPMS_Prod_URL_Path.json"); 
    // urlData = fs.readFileSync(parentPath + "aditems" + fspath.sep + "RPMS_Prod_URL_Path.json"); //Adjusted path to look in aditems folder
    file = "APITokenStorage.json";
    // url = "https://webapi.gorpms.com:8443/Token";
  }

  const apiURLPath = JSON.parse(urlData);
  const baseUrl = apiURLPath.url;

  let data = await fs.readFileSync(currentPath + file);
  let currentTokenInfo = JSON.parse(data);
  const dateValue = currentTokenInfo[".expires"]; // 'Sat, 26 Feb 2022 18:50:54 GMT'
  const currentTokenExpireDate = new Date(dateValue);
  const today = new Date();

  console.log("current token expires/expired on: " + currentTokenExpireDate);
  console.log("The current time is: " + today);

  // IF the token has expired go get a new one and store it in the .JSON file
  if (currentTokenExpireDate < today) {
    console.log("Current token has expired, grabbing new token.");
    console.log("Calling Token API for new token.");

    try {
      var grabTokenValue = await pjs.httpRequest({
        //Should replace sendRequest with httpRequest after we upgrade to > profound.js 6.1.0
        method: "POST",
        uri: baseUrl + "/Token",
        body: "grant_type=password&userName=mainframe%40pwadc.net&password=MySuperP@ssword!1",
        json: false,
        timeout: 5000,
        time: true
      });
    } catch (err) {
      console.log("error getting token. err:" + err);
      returnObj.errorMsg = err;
      return returnObj;
    }

    const newTokenValue = JSON.parse(grabTokenValue);

    await fs.writeFile(currentPath + file, grabTokenValue, err => {
      if (err) {
        console.error("error writting new token. err: " + err);
        returnObj.errorMsg = err;
        return returnObj;
      }
      console.log("APITokenStorage file updated.");
    });
    returnObj.value = newTokenValue.access_token;
  } else {
    // IF the token has not expired pass it along to the API call
    console.log("Current token has not expired and should be valid.");
    returnObj.value = currentTokenInfo.access_token;
  }
  return returnObj;
}
exports.run = getToken;
// exports.getToken = getToken;
